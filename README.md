# Quiebre Espontáneo de Simetría Local: El mecanismo de Higgs, caso no Abeliano

Este es un trabajo antiguo para la asignatura *Teoría Clásica de Campos*, donde
nos pedían desarrollar el mecanismo de Higgs para un campo vectorial complejo 
$`\phi`$ con $`n`$ entradas, es decir, bajo el grupo $`SU(n)`$.
