\documentclass[letterpaper,11pt,spanish]{fphw}

\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage[pdftex]{graphicx}
\usepackage[spanish]{babel}

\newcommand{\lp}{\left(}
\newcommand{\rp}{\right)}
\newcommand{\dlag}{ \ensuremath{ \mathcal{L} } }
\newcommand{\sun}{\ensuremath{SU(n)}}
\newcommand{\dagg}{^{\dagger}}
\newcommand{\tr}[1]{\mathrm{tr}\,(#1)}
\newcommand{\Fbbm}{\mathbb{F}}
\newcommand{\submu}{_{\mu}}

\title{Quiebre Espontáneo de Simetría Local, El Mecanismo de Higgs: Caso No-Abeliano}
\author{Felipe Portales Oliva}
\date{\today}
\institute{Universidad de Concepción \\ Ciencias Físicas y Astronómicas}
\professor{Dr. Juan Crisóstomo}
\class{Teoría Clásica de Campos}

\begin{document}

\maketitle



\begin{abstract}
En este trabajo para el curso de \textit{Teoría Clásica de Campos}, se muestra
el caso no Abeliano del mecanismo de Higgs, para una densidad Lagrangeana
invariante ante el grupo $SU(n)$ de donde se obtiene un campo escalar con
masa, cuya partícula asociada recibe el nombre de Bosón de Higgs y $2n-1$ campos
matriciales masivos.
\end{abstract}

\section{Introducción}

Consideremos $n$ campos escalares complejos $\phi_1,\ \phi_2, \ldots\,
\phi_n$, cuya dinámica esté gobernada por la densidad
Lagrangeana\footnote{Usamos la métrica de Minkowski con la signatura usada en
clases $\eta^{\mu\nu} = \mathrm{diag\,}(-1, 1, 1, 1)$ y unidades naturales:
$\hbar = 1$ y $c=1$.} 
\begin{equation}
 \dlag = - \frac{1}{2} \lp \partial_{\mu} \phi^{\dagger} \partial^{\mu} \phi  +
\mu^2 \phi^{\dagger}\phi + \frac{1}{2} |\lambda| (\phi^{\dagger}\phi)^2 \rp
\end{equation} donde hemos agrupado los campos en el vector columna
$$ \phi := \lp \begin{array}{c}
    \phi_1 \\
    \vdots \\
    \phi_n
   \end{array} \rp.
 $$

Esta densidad Lagrangeana es invariante ante el grupo de transformaciones
$SU(n)$ global, en efecto, si se considera un $U \in \sun$, con $ U $
independiente de las coordenadas del espacio de Minkowski y la transformación
$\bar{\phi} = U\phi$, se tiene que $\bar{\phi}\dagg = \phi\dagg U\dagg$ y por
tanto:
\begin{align}
 \bar{\phi}\dagg\bar{\phi} & = (\phi\dagg U\dagg)(U \phi) \nonumber \\
                           & = \phi\dagg (U\dagg U) \phi \nonumber \\
                           & = \phi\dagg (\mathbb{I}) \phi_1 \nonumber \\
                           & = \phi\dagg \phi \nonumber
\end{align}
de donde es inmediato que $(\bar{\phi}\dagg\bar{\phi})^2 = (\phi\dagg \phi)^2$.
Además, por un procedimiento análogo se ve que
$\partial_{\mu}\bar{\phi}\dagg\partial^{\mu}\bar{\phi} =
\partial_{\mu}\phi\dagg \partial^{\mu}\phi$. Luego, como todos los términos
afectados por la acción de \sun{} son invariantes, la densidad Lagrangeana es
también invariante, lo que mantiene a la acción 
$$S := \int_{M} \dlag \, d^4 x = - \frac{1}{2} \int_{M} \lp \partial_{\mu}
\phi^{\dagger} \partial^{\mu} \phi  + \mu^2 \phi^{\dagger}\phi + \frac{1}{2}
|\lambda| (\phi^{\dagger}\phi)^2 \rp \, d^4 x$$
invariante, es decir, la física no se ve alterada.

Ahora bien, la generalización de dicha densidad Lagrangeana, que mantiene la
invarianza bajo el grupo \sun{} local (es decir, donde $U\in\sun$ con $U =
U(x^{\lambda})$) es
\begin{equation}
 \dlag = - \frac{1}{2} \lp (D_{\mu} \phi)^{\dagger} (D^{\mu} \phi)  + \mu^2
\phi^{\dagger}\phi + \frac{1}{2} |\lambda| (\phi^{\dagger}\phi)^2 +
\frac{g}{2\pi}
\tr{ \Fbbm_{\mu\nu}\Fbbm^{\mu\nu} }\rp \label{ec:dens-lang-comp}
\end{equation}
donde hemos introducido la derivada covariante $D_{\mu}\phi :=
\partial_{\mu}\phi + iW_{\mu}\phi$, de manera tal que si definimos la
transformación $\bar{\phi} := U \phi$ se tiene:
\begin{equation}
 \bar{D}_{\mu} \bar{\phi} = U(D_{\mu}\phi) \quad \Longrightarrow \quad
(\bar{D}_{\mu} \bar{\phi})\dagg = (D_{\mu}\phi)\dagg U\dagg
\end{equation}
donde el campo de gauge $W\submu$ transforma como:
\begin{equation}
 \bar{W}\submu = U W\submu U\dagg - i U \partial\submu U\dagg \label{gauge}
\end{equation}
y la intensidad de campo de Yang - Mills se define por
\begin{equation}
 \Fbbm_{\mu\nu}:= D\submu W_{\nu} - D_{\nu} W\submu + i [W\submu, W_{\nu}]
\end{equation}
de manera que transforma como $\bar{\Fbbm}_{\mu\nu} = U \Fbbm_{\mu\nu} U\dagg$,
por lo que se tiene que la traza que aparece en la densidad Lagrangeana es un
escalar bajo el grupo \sun. En efecto:
\begin{align}
 \tr{\bar{\Fbbm}_{\mu\nu} \bar{\Fbbm}^{\mu\nu}} &= \tr{(U \Fbbm_{\mu\nu}
U\dagg)(U \Fbbm^{\mu\nu} U\dagg)} \nonumber\\
& = \tr{U \Fbbm_{\mu\nu} \Fbbm^{\mu\nu} U\dagg} \nonumber \\
& = \tr{U\dagg U \Fbbm_{\mu\nu} \Fbbm^{\mu\nu} } \nonumber \\
\therefore\ \tr{\bar{\Fbbm}_{\mu\nu} \bar{\Fbbm}^{\mu\nu}}  & =
\tr{\Fbbm_{\mu\nu} \Fbbm^{\mu\nu}}.
\end{align}

El campo de gauge $W\submu$ no es la causa de un posible quiebre de simetría,
ya que posee un valor esperado de vacío nulo, por lo que analizamos únicamente
el potencial efectivo $V$ del campo $\phi$ para buscar el estado de mínima
energía
\begin{equation}
 V(\phi\dagg\phi) = \frac{1}{2}\mu^{2} \phi\dagg\phi + \frac{1}{4} |\lambda|
(\phi\dagg\phi)^2. \label{potencial}
\end{equation}

Si buscamos puntos críticos del potencial (\ref{potencial}), derivando con
respecto a los campos escalares:
\begin{align*}
 \frac{\partial V}{\partial \phi_i}  = \frac{1}{2}\mu^{2} \phi_i^{*} +
\frac{1}{2} |\lambda| (\phi\dagg\phi) \phi_i^{*} , &\qquad i=1,\ldots,n  \\
 \frac{\partial V}{\partial \phi_i^{*}} = \frac{1}{2}\mu^{2} \phi_i +
\frac{1}{2} |\lambda| (\phi\dagg\phi) \phi_i , &\qquad i=1,\ldots,n
\end{align*}
de donde se ve que los puntos críticos quedan determinados por las condiciones
\begin{equation}
 \phi= \phi\dagg = 0, \qquad \phi\dagg\phi = - \frac{\mu^2}{|\lambda|}.
\end{equation}

Evidentemente $\phi\dagg\phi \geq 0$, por lo que los valores que tome el
parámetro $\mu^2$ definen la posibilidad física de la condición.

\section{Simetría Exacta: $\mu^2 \geq 0$}
Para este caso, la condición $\phi\dagg\phi = - {\mu^2}/{|\lambda|}$ no tiene
sentido, por lo que necesariamente el estado de vacío queda determinado por $$
\phi = \phi\dagg = 0, $$ es decir, éste es un mínimo de energía y el estado de
vacío del modelo es no degenerado e invariante ante \sun.

Al ser la densidad Lagrangeana invariante, vemos que se tiene un caso de
simetría exacta.

En el caso de pequeñas oscilaciones en las proximidades del estado de vacío
$(\phi\dagg\phi)^2 \ll \phi\dagg\phi$, por lo que la densidad Lagrangeana que
describe apropiadamente a este sistema reduce a
\begin{equation}
 \dlag_{po} = - \frac{1}{2} \lp (D_{\mu} \phi)^{\dagger} (D^{\mu} \phi)  + \mu^2
\phi^{\dagger}\phi  + \frac{g}{2\pi} \tr{ \Fbbm_{\mu\nu}\Fbbm^{\mu\nu} }\rp
\end{equation}
o escrito usando la definición de las derivadas covariantes
\begin{align}
 \dlag_{po} = & - \frac{1}{2} (\partial_{\mu} \phi)^{\dagger} (\partial^{\mu}
\phi)
 - \frac{1}{2}\mu^2 \phi^{\dagger}\phi  -\frac{i}{2}\lp(\partial\submu\phi)\dagg
W^{\mu}\phi - \phi\dagg W\dagg_{\mu}(\partial^{\mu}\phi) \rp  \nonumber \\
 & - \frac{1}{2}\phi\dagg W\dagg\submu W^{\mu}\phi - \frac{g}{4\pi} \tr{
\Fbbm_{\mu\nu}\Fbbm^{\mu\nu} } \label{ec:dens-lag-resum}
\end{align}
que describe $2n$ campos escalares cargados acoplados a un campo de gauge no
Abeliano que se manifiestan como $2n$ partículas de masa $\mu$ y fotones sin
masa respectivamente, los que se introducen en la teoría para mantener la
invarianza local.

Sin embargo este caso no resulta interesante para nuestro estudio puesto que no
se manifiesta el mecanismo de Higgs.

\section{Quiebre Espontáneo de Simetría: $\mu^2 < 0$}

En este caso podemos escribir: $\mu^2 = -|\mu|^2$ y por lo tanto la condición
$\phi\dagg\phi ={|\mu|^2}/{|\lambda|}$ define una infinidad de puntos
críticos. Si analizamos la naturaleza de los puntos críticos usando la segunda
derivada
$$ \frac{\partial^2 V}{\partial \phi_i^{*} \partial \phi_j}= \frac{\partial^2
V}{\partial \phi_j \partial \phi_i^{*}}  = -\frac{1}{2}|\mu|^{2} \delta_{ij} +
\frac{1}{2} |\lambda| \lp \delta_{ij}\phi\dagg\phi + \phi_j\phi_i^{*}\rp,$$
que para que sea un número real se debe tener $i=j$
$$\frac{\partial^2 V}{\partial \phi_i \partial \phi_i^{*}}  =
-\frac{1}{2}|\mu|^{2} n + \frac{1}{2} |\lambda| \lp n + 1\rp \phi\dagg\phi.$$

Ahora, si $\phi=0$:
$$\left. \frac{\partial^2 V}{\partial \phi_i \partial
\phi_i^{*}}\right|_{\phi = 0}  = -\frac{1}{2}|\mu|^{2} n < 0,$$
por lo tanto $\phi=\phi\dagg=0$ es un punto de máximo relativo de la energía.

Ahora si consideramos $\phi\dagg\phi ={|\mu|^2}/{|\lambda|}$ se tiene
$$\frac{\partial^2 V}{\partial \phi_i \partial \phi_i^{*}}  =
-\frac{1}{2}|\mu|^{2} n + \frac{1}{2} \cancel{|\lambda|} (n + 1)
\frac{|\mu|^2}{\cancel{|\lambda|}} = \frac{1}{2} {|\mu|^2} > 0$$
por lo que esta condición define una esfera de radio $R =
{|\mu|}/{\sqrt{|\lambda|}}$ en el espacio $n$-dimensional donde todos los
puntos que pertenecen a ella son estados de mínima energía; se dice entonces que
el vacío es infinitamente degenerado. Puesto que todos los puntos sobre ésta
esfera son estados de vacío, tenemos la libertad de escoger cualquiera de ellos
como fundamental. Por simplicidad elegimos
\begin{equation}
\left\langle\phi\right\rangle_0 := \lp 
\begin{array}{c}
    0 \\
    \vdots \\
    0 \\
    \nu
   \end{array} \rp.
\end{equation}
donde hemos definido:
\begin{equation}
  \nu := \frac{|\mu|}{\sqrt{|\lambda|}}. \label{ec:nu}
\end{equation}

Notamos que la acción de un elemento de \sun{} sobre el estado de vacío
escogido $\langle\phi\rangle_0$ lleva el estado a otro distinto (similar a una
rotación), por lo tanto el vacío no es invariante ante \sun{}, por lo que
estamos frente a un caso de ruptura espontánea de la simetría del sistema.

Definimos ahora un estado desplazado en un estado de vacío
\begin{equation}
 \phi' \equiv \lp 
             \begin{array}{c}
             \xi_1\\
             \vdots \\
             \xi_{n-1} \\
             \eta
             \end{array}
             \rp
:= \phi - \left\langle\phi\right\rangle_0 = 
             \lp 
             \begin{array}{c}
             \phi_1\\
             \vdots \\
             \phi_{n-1} \\
             \phi_n - \nu
             \end{array}
             \rp,
\end{equation}
o equivalentemente
$$  \phi   = \phi' + \left\langle\phi\right\rangle_0 = 
              \lp 
             \begin{array}{c}
             \xi_1\\
             \vdots \\
             \xi_{n-1} \\
             \eta + \nu
             \end{array}
             \rp.
$$

Como $\nu$ es una constante se tiene que 
\begin{equation}
\partial^{\mu}\phi   =  
              \lp 
             \begin{array}{c}
             \partial^{\mu}\xi_1\\
             \vdots \\
             \partial^{\mu}\xi_{n-1} \\
             \partial^{\mu}\eta
             \end{array}
             \rp
\quad
\mathrm{y}
\quad
\partial\submu\phi\dagg = \lp \partial_{\mu}\xi_1^{*}, \ldots,
\partial_{\mu}\xi_{n-1}^{*}, \partial_{\mu}\eta^{*} \rp,
\end{equation}
por lo que si definimos el índice $i=1,\ldots,n-1$, se tiene
\begin{equation}
 \partial\submu\phi\dagg \partial^{\mu}\phi = \partial_{\mu}\xi_i^{*}
\partial^{\mu}\xi_i + \partial_{\mu}\eta^{*} \partial^{\mu}\eta.
\label{eq:derivadas}
\end{equation}

Por otra parte
$$ \phi\dagg\phi = \xi_{i}^{*}\xi_{i} + (\eta^{*}+\nu)(\eta+\nu) =
\xi_{i}^{*}\xi_{i} + |\eta|^2 + \nu(\eta+\eta^{*}) + \nu^2 $$
entonces
\begin{align*}
(\phi\dagg\phi)^2 = & \ (\xi_{i}^{*}\xi_{i})^2 + |\eta|^4 +
\nu^2(\eta+\eta^{*})^2 + \nu^4 + 2\nu^2|\eta|^2 + 2\nu|\eta|^2 (\eta+\eta^{*})
\\
 & + 2\nu^{3}(\eta+\eta^{*}) + 2|\eta|^2\xi_{i}^{*}\xi_{i} +
2\nu\xi_{i}^{*}\xi_{i}(\eta+\eta^{*}) + 2\nu^2\xi_{i}^{*}\xi_{i}.
\end{align*}

Además
$$ \partial\submu\phi\dagg W^{\mu}\phi = {\partial\submu\phi'}\dagg W^{\mu}\phi'
+ {\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0, $$
$$ \phi\dagg W\submu\dagg \partial^{\mu}\phi = {\phi'}\dagg W\submu\dagg
\partial^{\mu}\phi' + \langle\phi\rangle_0\dagg W\submu\dagg
\partial^{\mu}\phi'$$
$$ \mathrm{y}\quad \phi\dagg W\dagg\submu W^{\mu} \phi =  {\phi'}\dagg
W\dagg\submu W^{\mu}{\phi'} + \langle\phi\rangle_0\dagg
W\dagg\submu W^{\mu}{\phi'} + {\phi'}\dagg
W\dagg\submu W^{\mu}\langle\phi\rangle_0 + \langle\phi\rangle_0\dagg
W\dagg\submu W^{\mu}\langle\phi\rangle_0.$$

Si ahora aprovechamos los resultados de (\ref{ec:dens-lag-resum}) para expandir
la densidad Lagrangeana (\ref{ec:dens-lang-comp}) obtenemos
\begin{align}
 \dlag = & - \frac{1}{2} (\partial_{\mu} \phi)^{\dagger} (\partial^{\mu} \phi)
 - \frac{1}{2}\mu^2 \phi^{\dagger}\phi - \frac{1}{4}|\lambda|
(\phi^{\dagger}\phi)^2 - \frac{1}{2}\phi\dagg W\dagg\submu
W^{\mu}\phi \nonumber \\ 
 & -\frac{i}{2}\lp(\partial\submu\phi)\dagg W^{\mu}\phi - \phi\dagg
W\dagg_{\mu}(\partial^{\mu}\phi) \rp  - \frac{g}{4\pi} \tr{
\Fbbm_{\mu\nu}\Fbbm^{\mu\nu} } 
\end{align}
que al reemplazar lo obtenido anteriormente, recordando de la definición de
$\nu$ dada en (\ref{ec:nu}) que podemos escribir
$$ |\lambda| = \frac{|\mu|^2}{\nu^2} $$
se obtiene
\begin{align}
 \dlag = 
% cinético
& -\frac{1}{2} (\partial_{\mu}\xi_i^{*} \partial^{\mu}\xi_i +
\partial_{\mu}\eta^{*} \partial^{\mu}\eta) 
% acoplamiento campo EM
- \frac{g}{4\pi} \tr{\Fbbm_{\mu\nu}\Fbbm^{\mu\nu} } \nonumber \\
% término feo mixto derivadas y campos
& -\frac{i}{2} \lp {\partial\submu\phi'}\dagg W^{\mu}\phi'
+ {\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0 -  {\phi'}\dagg
W\submu\dagg
\partial^{\mu}\phi' - \langle\phi\rangle_0\dagg W\submu\dagg
\partial^{\mu}\phi'\rp \nonumber \\
% término feo con gauge cuadrado
& -\frac{1}{2}\lp {\phi'}\dagg W\dagg\submu W^{\mu}{\phi'} +
\langle\phi\rangle_0\dagg
W\dagg\submu W^{\mu}{\phi'} + {\phi'}\dagg
W\dagg\submu W^{\mu}\langle\phi\rangle_0 + \langle\phi\rangle_0\dagg
W\dagg\submu W^{\mu}\langle\phi\rangle_0\rp \nonumber \\
% término cuadrado en campos phi
& + \frac{1}{2}|\mu|^2 \lp \cancel{\xi_{i}^{*}\xi_{i}} + \cancel{|\eta|^2} +
\cancel{\nu(\eta+\eta^{*})} + \nu^2 \rp \nonumber \\
% término cuártico en campos phi
& - \frac{|\mu|^2}{4\nu^2} \lp (\xi_{i}^{*}\xi_{i})^2 + |\eta|^4 +
\nu^2(\eta+\eta^{*})^2 + \nu^4 + \cancel{2\nu^2|\eta|^2} + 2\nu|\eta|^2
(\eta+\eta^{*}) \right. \nonumber \\
 & \left. + \cancel{2\nu^{3}(\eta+\eta^{*})} + 2|\eta|^2\xi_{i}^{*}\xi_{i} +
2\nu\xi_{i}^{*}\xi_{i}(\eta+\eta^{*}) + \cancel{2\nu^2\xi_{i}^{*}\xi_{i}} \rp.
\end{align}

Reduciendo y agrupando términos (ya que en la forma presentada está muy
feo) se sigue que la densidad Lagrangeana es entonces

\begin{align}
  \dlag = 
% bosón de higgs
& -\frac{1}{2} \lp \partial_{\mu}\eta^{*} \partial^{\mu}\eta +
\frac{|\mu|^2}{2}(\eta+\eta^{*})^2 \rp 
% campos cuadráticos
-\frac{1}{2} \partial_{\mu}\xi_i^{*}\partial^{\mu}\xi_i 
% acoplamiento campo EM
- \frac{g}{4\pi} \tr{\Fbbm_{\mu\nu}\Fbbm^{\mu\nu} } \nonumber \\
% término feo mixto derivadas y campos
& -\frac{i}{2} \lp {\partial\submu\phi'}\dagg W^{\mu}\phi'
+ {\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0 -  {\phi'}\dagg
W\submu\dagg
\partial^{\mu}\phi' - \langle\phi\rangle_0\dagg W\submu\dagg
\partial^{\mu}\phi'\rp \nonumber \\
% término feo con gauge cuadrado
& -\frac{1}{2}\lp {\phi'}\dagg W\dagg\submu W^{\mu}{\phi'} +
\langle\phi\rangle_0\dagg
W\dagg\submu W^{\mu}{\phi'} + {\phi'}\dagg
W\dagg\submu W^{\mu}\langle\phi\rangle_0 + \langle\phi\rangle_0\dagg
W\dagg\submu W^{\mu}\langle\phi\rangle_0\rp \nonumber \\
% términos restantes
& -\frac{1}{2} |\mu| ^2\lp \frac{1}{\nu}(\eta+\eta^{*})(|\eta|^2 +
\xi_{i}^{*}\xi_{i}) - \frac{1}{2\nu^2} \lp (\xi_{i}^{*}\xi_{i})^2 + |\eta|^4 +
2(\xi_{i}^{*}\xi_{i})|\eta|^2 \rp \rp.
\end{align}

Vemos que en la densidad Lagrangeana aparecen términos de orden cúbico en las
componentes de $\phi'$, los causantes de la ruptura de simetría.

En el caso de estar interesados en pequeñas oscilaciones alrededor del estado
de vacío, es decir, $|\phi'| \ll 1$, los términos de orden 3 y superiores en los
campos, tanto $\phi'$ como los campos de gauge (puesto que su valor de
expectación en el vacío es nulo), se vuelven irrelevantes en el estudio, al
igual que los términos constantes que no influyen en las ecuaciones de campo del
sistema.

Haciendo estas consideraciones, podemos escribir la densidad Lagrangeana para
pequeñas oscilaciones como 
\begin{align}
  \dlag_{po} = 
% bosón de higgs
& -\frac{1}{2} \lp \partial_{\mu}\eta^{*} \partial^{\mu}\eta +
\frac{|\mu|^2}{2}(\eta+\eta^{*})^2 \rp 
% campos cuadráticos
-\frac{1}{2} \partial_{\mu}\xi_i^{*}\partial^{\mu}\xi_i 
% acoplamiento campo EM
- \frac{g}{4\pi} \tr{\Fbbm_{\mu\nu}\Fbbm^{\mu\nu} } \nonumber \\
% término feo mixto derivadas y campos
& -\frac{i}{2} \lp  {\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0  -
\langle\phi\rangle_0\dagg W\submu\dagg \partial^{\mu}\phi'\rp 
% término feo con gauge cuadrado
-\frac{1}{2}\langle\phi\rangle_0\dagg W\dagg\submu W^{\mu}\langle\phi\rangle_0.
\label{lagrangeano}
\end{align}

Ahora bien, por comodidad hacemos los siguientes cambios de variable:
$$ \alpha : = \mathrm{Re}\, \eta, \qquad \beta : =
\mathrm{Im}\, \eta, $$
por lo que es sencillo ver que $\eta + \eta^{*} = 2\alpha$ y además
\begin{equation}
\partial_{\mu}\eta^{*} \partial^{\mu}\eta = \partial_{\mu}\alpha \partial^{\mu}\alpha +
\partial_{\mu}\beta\partial^{\mu}\beta. \label{asdf}
\end{equation}

Si analizamos el término ${\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0  -
\langle\phi\rangle_0\dagg W\submu\dagg \partial^{\mu}\phi'$ con cuidado, usando la notación de índices, donde $a,b = 1,\ldots,n$ se tiene
$$ \langle\phi\rangle_{0}^a = \nu \delta_{n}^a, \quad
\partial^{\mu}{\phi'}^a=
\left\{\begin{array}{l l}
\partial^{\mu}\xi_a\quad & \mathrm{si\ }a=1,\ldots,n-1.\\
\partial^{\mu}\eta &\mathrm{si\ }a=n.\\
\end{array}\right. $$
$$ \langle\phi\rangle_{0a} = \nu \delta^{n}_a, \quad
\partial_{\mu}{\phi'}\dagg_a=
\left\{\begin{array}{l l}
\partial_{\mu}\xi_a^{*} \quad & \mathrm{si\ }a=1,\ldots,n-1.\\
\partial_{\mu}\eta^{*} &\mathrm{si\ }a=n.\\
\end{array}\right. $$

Por lo que podemos expresar la suma como 
\begin{align*}
{\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0  - \langle\phi\rangle_0\dagg W\submu\dagg \partial^{\mu}\phi' 
& = \nu (-\partial\submu\xi_i [W^{\mu}]^i_n + [W\submu\dagg]^n_i \partial^{\mu}\xi_i + {[W\submu]^n_n}^{*} \partial^{\mu} \eta - [W^{\mu}]^n_n \partial\submu\eta^{*} ) \\
& = \nu (-\partial\submu\xi_i [W^{\mu}]^i_n + [W\submu\dagg]^n_i \partial^{\mu}\xi_i + {[W\submu]^n_n}^{*} \partial^{\mu} \eta - [W_{\mu}]^n_n \partial^{\mu}\eta^{*} )
\end{align*}
de donde vemos que la parte real del campo escalar $\eta$ no aparece en el término, si la componente de la fila $n$ y de la columna $n$ de la matriz $W_\mu$ es real, condición que podemos imponer sin que sea sobre-exigir al sistema, por la libertad impuesta para la definición del campo de gauge. El hecho de que no aparezca el término $\alpha = \mathrm{Re}\,\eta$ permite definir el vector de los campos escalares que interactúan con el campo de gauge:
\begin{equation}
\varphi := \lp \begin{array}{c}
\xi_1 \\
\vdots \\
\xi_{n-1} \\
i\beta
\end{array} \rp 
\end{equation}
de manera tal que podamos hacer el reemplazo $\phi' \rightarrow \varphi$ en el término anterior
\begin{equation}
{\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0  - \langle\phi\rangle_0\dagg W\submu\dagg \partial^{\mu}\phi' = {\partial\submu\varphi}\dagg W^{\mu}\langle\phi\rangle_0  - \langle\phi\rangle_0\dagg W\submu\dagg \partial^{\mu}\varphi .
\end{equation}

Notemos ahora que se tiene 
\begin{align}
\partial_{\mu}\xi_i^{*}&\partial^{\mu}\xi_i +  \partial_{\mu}\beta\partial^{\mu}\beta + i{\partial\submu\phi'}\dagg W^{\mu}\langle\phi\rangle_0  -
i\langle\phi\rangle_0\dagg W\submu\dagg \partial^{\mu}\phi' + {\langle\phi\rangle}_0\dagg W\dagg\submu W^{\mu}{\langle\phi\rangle}_0  \nonumber \\
& = \partial_{\mu}\varphi\dagg\partial^{\mu}\varphi + i{\partial\submu\varphi}\dagg W^{\mu}\langle\phi\rangle_0  - i\langle\phi\rangle_0\dagg W\submu\dagg \partial^{\mu}\varphi + {\langle\phi\rangle}_0\dagg W\dagg\submu W^{\mu}{\langle\phi\rangle}_0  \nonumber \\
& = {\langle\phi\rangle}_0\dagg \lp 
\frac{1}{\nu^4} {\langle\phi\rangle}_0 \partial_{\mu}\varphi\dagg\partial^{\mu}\varphi {\langle\phi\rangle}_0\dagg + \frac{i}{\nu^2} {\langle\phi\rangle}_0 {\partial\submu\varphi}\dagg W^{\mu} - \frac{i}{\nu^2} W\submu\dagg \partial^{\mu}\varphi {\langle\phi\rangle}_0\dagg + W\dagg\submu W^{\mu}
 \rp {\langle\phi\rangle}_0 \nonumber \\
& = {\langle\phi\rangle}_0\dagg \lp
W\submu\dagg + \frac{i}{\nu^2} {\langle\phi\rangle}_0 \partial_{\mu}\varphi\dagg
\rp\!\! \lp
W^{\mu} - \frac{i}{\nu^2}\partial^{\mu}\varphi {\langle\phi\rangle}_0\dagg
 \rp {\langle\phi\rangle}_0 \nonumber \\
& = \overline{\langle\phi\rangle}_0\dagg \bar{W}\submu\dagg \bar{W}^{\mu} \overline{\langle\phi\rangle}_0 
\end{align}
donde hemos definido 
\begin{equation}
\bar{W}\submu := U\lp W\submu - \frac{i}{\nu^2} \partial\submu\varphi \langle\phi\rangle\dagg_0 \rp U\dagg, \label{newgauge}
\end{equation}
con $U=U(x^{\lambda})\in\sun$ que satisface la ecuación diferencial $\partial\submu U\dagg = \frac{i}{\nu^2} (\partial\submu\varphi) \langle\phi\rangle\dagg_0 U\dagg$. Este es un requerimiento para que la  definición dada sea una transformación de gauge admisible como la descrita en la ecuación (\ref{gauge}).
Además hemos usado $\overline{\langle\phi\rangle}_0 := U {\langle\phi\rangle}_0$ y $\overline{\langle\phi\rangle}_0\dagg = {\langle\phi\rangle}_0\dagg U\dagg$.


Si aplicamos entonces lo obtenido en (\ref{asdf}) y (\ref{newgauge}) en la densidad Lagrangeana (\ref{lagrangeano}) vemos que
\begin{equation}
  \dlag_{po} = 
% bosón de higgs
-\frac{1}{2} \lp \partial_{\mu}\alpha \partial^{\mu}\alpha + 2{|\mu|^2}\alpha^2 \rp 
% campos cuadráticos
- \overline{\langle\phi\rangle}_0\dagg \bar{W}\submu\dagg \bar{W}^{\mu} \overline{\langle\phi\rangle}_0
% acoplamiento campo EM
- \frac{g}{4\pi} \tr{\Fbbm_{\mu\nu}\Fbbm^{\mu\nu} } .
\end{equation}
donde tenemos lo siguiente:
\begin{itemize}
\item Un campo $\alpha$ con masa $m = \sqrt{2}|\mu|>0$.
\item $2n-1$ campos matriciales masivos contenidos en $\bar{W}_{\mu}$ con masa $\nu$ (escondida en los vectores $\overline{\langle\phi\rangle}_0$).
\item Los campos $\xi_i$ y $\beta = \mathrm{Im}\,\eta$ desaparecen de la densidad Lagrangeana.
\end{itemize}

\section{Resultados}
En el lenguaje común se dice que los campos de gauge se comen a los campos $\xi_i$ y $\beta = \mathrm{Im}\,\eta$ y se convierten en bosones matriciales masivos. El campo caracterizado por $\alpha$ se conoce como bosón de Higgs. La propiedad de los gauge que se manifiesta en la absorción de los campos $\xi_i$ y $\beta$ es lo que se conoce como mecanismo de Higgs.


\end{document}
